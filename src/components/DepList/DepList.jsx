import React from 'react';
import PropTypes from 'prop-types';
import style from './DepList.css';
import DepContainer from '../../containers/DepContainer';
import renderTree from '../../utils/renderTree';

const DepList = ({ departments }) => (
  <div className={style.deplist}>
    <ul className={style.deplist__list}>{renderTree(departments, DepContainer)}</ul>
  </div>
);

DepList.propTypes = {
  departments: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default DepList;
