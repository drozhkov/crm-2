export const SORT_EMPLOYEE_LIST = 'SORT_EMPLOYEE_LIST';
export const FILTER_EMPLOYEE_LIST_BY_DEPARTMENT = 'FILTER_EMPLOYEE_LIST_BY_DEPARTMENT';
export const FILTER_EMPLOYEE_LIST_BY_VACATION = 'FILTER_EMPLOYEE_LIST_BY_VACATION';
export const SET_SEARCH_TERM = 'SET_SEARCH_TERM';
export const DEPARTMENT_ID = 'DEPARTMENT_ID';
export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';

export const ID = 'id';
export const NAME = 'name';
export const ID_TITLE = 'Id';
export const NAME_TITLE = 'Name';

export const DIT_DATA = 'ditData';

export const SERVER_API_URL = 'http://localhost:3002/api';

export const FIELD_IS_REQUIRED_MSG = 'This field is required';
export const INVALID_EMAIL_MSG = 'Invalid email address';
