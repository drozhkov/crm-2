import { connect } from 'react-redux';
import Dep from '../components/Dep/Dep';
import { setEmployeeListFilterByDepartment } from '../actionCreators';

const mapStateToProps = state => ({
  filterByDepartment: state.filterByDepartment
});

const mapDispatchToProps = dispatch => ({
  clickHandler: filterBy => dispatch(setEmployeeListFilterByDepartment(filterBy))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dep);
