import { FILTER_EMPLOYEE_LIST_BY_DEPARTMENT, FILTER_EMPLOYEE_LIST_BY_VACATION } from '../constants';

export function filterEmployeeListByDepartment(state = 1, action) {
  if (action.type === FILTER_EMPLOYEE_LIST_BY_DEPARTMENT) {
    return action.payload;
  }
  return state;
}

export function filterEmployeeListByVacation(state = false, action) {
  if (action.type === FILTER_EMPLOYEE_LIST_BY_VACATION) {
    return action.payload;
  }
  return state;
}
