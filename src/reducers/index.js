import { combineReducers } from 'redux';
import sortEmployeeList from './sortEmployeeList';
import { filterEmployeeListByDepartment, filterEmployeeListByVacation } from './filterEmployeeList';
import setSearchTerm from './setSearchTerm';
import authorization from './authorization';

const rootReducer = combineReducers({
  sortBy: sortEmployeeList,
  filterByDepartment: filterEmployeeListByDepartment,
  filterByVacation: filterEmployeeListByVacation,
  searchTerm: setSearchTerm,
  authorized: authorization
});

export default rootReducer;
